SH=`dirname $(realpath ${BASH_SOURCE:-$0})`
GH=`realpath $SH/..`
cd $GH

# get :python3 3.9.x via pyenv
pyenv install --skip-existing 3.9 ; pyenv local 3.9

#get pipenv latest of current :python3
python3 -m ensurepip --upgrade
python3 -m       pip install --upgrade pipenv

#install python pkg w/ pipenv
echo;echo;  rm -rf `python3 -m pipenv --venv`; python3 -m pipenv install
#           rm     previous .venv ifany ref. https://github.com/pypa/pipenv/issues/5052#issuecomment-1102331608

echo;echo;  python3 -V ; python3 -m pipenv --venv  # should see 3.9.x ; .../.venv
