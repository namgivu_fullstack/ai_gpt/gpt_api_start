from service.openai_gpt import translate

def run_test(inp, EXP_out):
    r, r_infull = translate(inp)

    print(r_infull)
    print()
    print(r)

    assert r == EXP_out


class Test:

    def test_1911(self): run_test(inp='What is the taxi fuel at SIN?',                                     EXP_out='select AVG(taxi_out) as taxi_out from table3_f where dep = SIN')
    def test_1922(self): run_test(inp='What was the historical weather delay at dest over the past year?', EXP_out='select AVG(last_3m_wx) as last_3m_wx from new_arrDelays where station = dest and ETA = eta')
