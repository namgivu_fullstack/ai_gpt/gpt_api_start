import openai
#
from dotenv import load_dotenv
import os
#
import re


load_dotenv()
OPENAI_API_KEY = os.environ.get('OPENAI_API_KEY'); assert OPENAI_API_KEY, 'Envvar OPENAI_API_KEY ie openai.api_key is required'
openai.api_key     = OPENAI_API_KEY


OPENAI_GPT_engine      = 'davinci'
OPENAI_GPT_max_tokens  = 100
OPENAI_GPT_temperature = 0.5
OPENAI_GPT_top_p       = 1
OPENAI_GPT_n           = 1
OPENAI_GPT_stream      = False

PROMPT_INP_prefix='input: ';  PROMPT_INP_suffix='\n'
PROMPT_OUT_prefix='output: '; PROMPT_OUT_suffix='\n\n'


##region build PRIMED_PROMPTS str from EXAMPLE_DATA list

#region parse :rows_copied_fr_spreadsheet_str to :row_list
rows_copied_fr_spreadsheet_str='''
Free text query	SQL query
some text 00    expected result 00
some text 01    expected result 01
some text 02    expected result 02
'''.strip()
splitted      = rows_copied_fr_spreadsheet_str.split('\n')
# header_list = splitted[0] ; [plaintext, sqlquery] = header_list.split('\t')
row_list      = splitted[1:]
row_list      = [                                      r.split('\t') for r in row_list ]
# row_list    = [ {plaintext:s[0], sqlquery:s[1] } for s                   in row_list ]
row_list      = [ {    'inp':s[0],    'out':s[1] } for s                   in row_list ]
#endregion parse :rows_copied_fr_spreadsheet_str to :row_list

EXAMPLE_DATA = row_list

PRIMED_PROMPTS = ''.join([
    PROMPT_INP_prefix + e['inp'] + PROMPT_INP_suffix +
    PROMPT_OUT_prefix + e['out'] + PROMPT_OUT_suffix
    for e in EXAMPLE_DATA
])
##endregion build PRIMED_PROMPTS str from EXAMPLE_DATA list


def translate(plain_text_query):

    def make_prompt_param(plain_text_query):
        prompt = plain_text_query  # clearer varname @ enduser's query as plaintext is a :prompt in gpt

        r =   PRIMED_PROMPTS \
            + PROMPT_INP_prefix + prompt + PROMPT_INP_suffix

        return r

    res = openai.Completion.create(
        prompt      = make_prompt_param(plain_text_query),

        engine      = OPENAI_GPT_engine,
        max_tokens  = OPENAI_GPT_max_tokens,
        temperature = OPENAI_GPT_temperature,
        top_p       = OPENAI_GPT_top_p,
        n           = OPENAI_GPT_n,
        stream      = OPENAI_GPT_stream,
    )

    r        = res['choices'][0]['text']  # return only the reply/choice ontop
    r_infull = res

    # recook r as gpt-returned val is too verbose
    m_all = re.findall('output: (.+)\n', r, re.MULTILINE)
    r     =                     m_all[0]

    return r, r_infull
